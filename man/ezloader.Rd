% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/ezloader.R
\name{ezloader}
\alias{ezloader}
\title{Load or install and load required packages}
\usage{
ezloader(
  pkg,
  lib = NULL,
  type = getOption("pkgType"),
  quiet = TRUE,
  msgs = -1,
  print_msgs = TRUE,
  with_return_value = FALSE
)
}
\arguments{
\item{pkg}{Package name}

\item{lib}{Library directory to use (default is .libPaths())}

\item{type}{Package type to use in install (optional)}

\item{quiet}{Boolean for output of install and require messages. Default is
TRUE}

\item{msgs}{Int to control global messages. Default is -1.}

\item{print_msgs}{Boolean to control if the function will print a loaded
message. Default is TRUE}

\item{with_return_value}{Boolean to control with the function should return a
value. Default is TRUE}
}
\value{
Boolean if with_return_value is TRUE, returns TRUE if successfully
loaded pkg.
}
\description{
This function will load a library. If it is not present in the system library
(.libPaths()), it will try to install it, with all the dependencies, and load
it. Amount of feedback can be controlled with the parameters in the function
call. It is also possible to control if the function will return or not a
boolean.
}
