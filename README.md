[![Built with Spacemacs](https://cdn.rawgit.com/syl20bnr/spacemacs/442d025779da2f62fc86c2082703697714db6514/assets/spacemacs-badge.svg)](https://develop.spacemacs.org)

# EZ Loader 

This package can replace `library()` or `require()`. It will load a package and, if it is not installed, it will install, with all the dependencies, and load it.

It is possible to control the type of package to install (source vs. binary), the amount of output information, and a return value (TRUE or FALSE if package was loaded or not).

## Instructions

Install with: 

```R
devtools::install_git("https://gitlab.com/christianvl/ez-loader.git")
```

Using this package is very simple:

``` R
ezloader("package_name")
```

The complete function call is:

``` R
ezloader(pkg,
        lib = NULL
        type = getOption("pkgType"),
        quiet = TRUE,
        msgs = -1,
        print_msgs = TRUE,
        with_return_value = FALSE)
```

## Parameters

The package can accept the following parameters:

- `pkg` = String with the package name.
- `lib` = Library folder to use. Defaults to `NULL` = `.libPaths()`
- `type` = install type (source, binary). Optional. Defaults to `getOption("pkgType")`.
- `quiet` = Suprress messages from `require()` and `install.packages()`. Default is `TRUE`.
- `msgs` = Turn off messages globally. System value is restored after function execution. Default is `-1` (no messages).
- `print_msgs` = Prints a String informing if the package was loaded, with the library path, or not. Default is `TRUE`.
- `with_return_value` = Points if the function should return a boolean for success (`TRUE`) or fail (`FALSE`). Default is `FALSE`.

## License
This software is licensed under the [GPL3](https://www.gnu.org/licenses/gpl-3.0.html) license.
![GPL](https://www.gnu.org/graphics/gplv3-88x31.png "GPL3")
